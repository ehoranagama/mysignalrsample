﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Hosting;
using Owin;
using System;

namespace MySignalRHub
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            string url = "http://localhost:8080/";
           
            using (WebApp.Start<Startup>(url))
            {
                Console.WriteLine("Server running on {0}",url);
                Console.ReadLine();
            }
        }
    }

    class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HubConfiguration() {EnableCrossDomain = true};
            appBuilder.MapHubs(config);
        }
    }

    [HubName("myHub")]
    public class MyHub : Hub
    {
        public string Send(string msg)
        {
            return msg;
        }

        public void DoSometing(string msg)
        {
            Console.WriteLine(msg);
            Clients.All.addMessage(msg);
        }
    }
}
