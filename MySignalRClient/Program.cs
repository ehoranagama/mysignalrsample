﻿using System;
using Microsoft.AspNet.SignalR.Client.Hubs;

namespace MySignalRClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var connection = new HubConnection("http://localhost:8080/");
            var myHub = connection.CreateHubProxy("myHub");

            //connection.Start().Wait();
            connection.Start().ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    Console.WriteLine("Error opening the connection:{0}",
                                      t.Exception.GetBaseException());
                }
                else
                    Console.WriteLine("Connected!");

            }).Wait();

            myHub.Invoke<string>("Send", "Hi ").ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("There was an error calling send: {0}",
                                      task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine(task.Result);
                }
            });

            myHub.On<string>("addMessage", param =>
            {
                Console.WriteLine(param);
            });

            myHub.Invoke<string>("DoSometing", "Something going on...!!!");

            Console.Read();
            connection.Stop();
        }
    }
}
