﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.AspNet.SignalR.Client.Hubs;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private HubConnection connection;// = new HubConnection("http://localhost:8080/");
        private IHubProxy myHub;

        private string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                OnPropertyChanged("Message");
            }
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (connection != null)
            {
                connection.Start().Wait();
                myHub.Invoke<string>("DoSometing", "Something is going on...!!!");
            }
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this;

            connection = new HubConnection("http://localhost:8080/");
            myHub = connection.CreateHubProxy("myHub");
            myHub.On<string>("addMessage", param =>
            {
                this.Message = (this.Message + Environment.NewLine + param);
            });
        }

        private void MainWindow_OnUnloaded(object sender, RoutedEventArgs e)
        {
            if (connection != null)
            {
                connection.Stop();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
